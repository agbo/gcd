//
//  GCDViewController.swift
//  GCD
//
//  Created by Fernando Rodríguez Romero on 9/6/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import UIKit

class GCDViewController: UIViewController {

    let url = URL(string: "https://orig02.deviantart.net/c3e4/f/2012/194/2/0/house_targaryen_by_archaox-d574ytp.jpg")!
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBAction func updateAlpha(_ sender: UISlider) {
        
        let value = CGFloat(sender.value)
        
        imageView.alpha = value
        
    }
    @IBAction func actorDownload(_ sender: AnyObject) {
        
        // clausura que cuelga
        withURL(url) { (img: UIImage) in
            self.imageView.image = img
        }
        
        // clausura como parámetro más
        withURL(url,completion: { (img : UIImage) in
            
        })
        
    }
    
    
    @IBAction func asyncDownload(_ sender: AnyObject) {
        
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            var data : Data
            do{
                try data = Data(contentsOf: self.url)
                
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                }
                
            }catch{
                print("Se jodió el invento")
            }
        }
        
        
        
        
        
        
        
    }
    
    
    
    @IBAction func syncDownload(_ sender: AnyObject) {
        
        
        var data : Data
        do{
            try data = Data(contentsOf: url)
            imageView.image = UIImage(data: data)
        }catch{
            print("Se jodió el invento")
        }
        
    }
    
    
    
    typealias completionClosure = (UIImage)->()
    
    func withURL(_ url : URL, completion : completionClosure){
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            do{
                let data = try Data(contentsOf: url)
                let img = UIImage(data: data)!
                
                // Hemos terminado, toca ejecutar la clausura
                // por conveción, la clausura de finalización
                // siempre en cola principal.
                DispatchQueue.main.async {
                    completion(img)
                }
                
            }catch{
                print("la cagamos")
            }
        }
        
    }
    
}
